-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: sql1.njit.edu
-- Generation Time: Jun 21, 2019 at 04:38 AM
-- Server version: 5.5.29-log
-- PHP Version: 5.6.37

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `df227`
--

-- --------------------------------------------------------

--
-- Table structure for table `exam_list`
--

CREATE TABLE IF NOT EXISTS `exam_list` (
`exam_id` int(11) NOT NULL,
  `exam_name` text NOT NULL,
  `question_list` text NOT NULL,
  `question_points` text NOT NULL,
  `max_points` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=72 ;

--
-- Dumping data for table `exam_list`
--

INSERT INTO `exam_list` (`exam_id`, `exam_name`, `question_list`, `question_points`, `max_points`) VALUES
(70, 'The Last Hoorah', '[{"qid":"73"},{"qid":"76"},{"qid":"79"}]', '[{"point_value":"25"},{"point_value":"50"},{"point_value":"25"}]', 100),
(71, 'Inshallah u pass', '[{"qid":"73"},{"qid":"74"},{"qid":"75"}]', '[{"point_value":"25"},{"point_value":"25"},{"point_value":"50"}]', 100);

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `ucid` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `id` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`ucid`, `password`, `id`) VALUES
('stud', '264c8c381bf16c982a4e59b0dd4c6f7808c51a05f64c35db42cc78a2a72875bb', 0),
('teach', '1057a9604e04b274da5a4de0c8f4b4868d9b230989f8c8c6a28221143cc5a755', 1);

-- --------------------------------------------------------

--
-- Table structure for table `question_bank`
--

CREATE TABLE IF NOT EXISTS `question_bank` (
`qid` int(255) NOT NULL,
  `question` text NOT NULL,
  `q_type` text NOT NULL,
  `function_name` varchar(255) NOT NULL,
  `q_difficulty` text NOT NULL,
  `q_cases` varchar(700) NOT NULL,
  `keyword` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=80 ;

--
-- Dumping data for table `question_bank`
--

INSERT INTO `question_bank` (`qid`, `question`, `q_type`, `function_name`, `q_difficulty`, `q_cases`, `keyword`) VALUES
(73, 'Write a function named doubleIt that takes in a number and returns twice the amount.', 'Other', 'doubleIt', 'easy', 'Mg==,NA==;Mw==,Ng==', ''),
(74, 'Write a function named stringMult that takes a number and a string. It must return the string printed that number of times.', 'Strings', 'stringMult', 'easy', 'J2hlbGxvJywy,aGVsbG9oZWxsbw==;J2hpJywz,aGloaWhp', ''),
(75, 'Write a function named plusTen that takes in a number and adds 10 to it. However it MUST use a for loop.', 'For Loops', 'plusTen', 'easy', 'MTA=,MjA=;MTU=,MjU=', 'for'),
(76, 'Write a function called op that takes in an operation and two numbers. The operations can be addition, multiplication, subtraction, or division. The program must be able to account for these possibilities.', 'If Statements', 'op', 'easy', 'JysnLDIsMw==,NQ==;Jy0nLDMsMg==,MQ==;JyonLDQsMw==,MTI=;Jy8nLDE1LDU=,Mw==', ''),
(77, 'Write a function named minusFive that takes a number and subtracts it by 1 five times. It MUST use a while loop.', 'While Loops', 'minusFive', 'easy', 'NQ==,MA==;MTA=,NQ==', 'while'),
(78, 'create nothing() that returns nothing', 'Other', 'nothing()', 'easy', 'Mg==,;J0hpJw==,', ''),
(79, 'Write a function named stringMultiply that takes a string and a number, and prints.', 'Other', 'stringMultiply', 'medium', 'ImhpIiwy,aGloaQ==;J2hlbGxvJywz,aGVsbG9oZWxsb2hlbGxv', 'for');

-- --------------------------------------------------------

--
-- Table structure for table `student_ans_exam`
--

CREATE TABLE IF NOT EXISTS `student_ans_exam` (
`uniq_exam_solve_id` int(11) NOT NULL,
  `ucid` varchar(255) NOT NULL,
  `exam_id` int(11) NOT NULL,
  `auto_grader_status` tinyint(1) NOT NULL DEFAULT '0',
  `graded_status` int(11) NOT NULL,
  `visible_status` int(11) NOT NULL,
  `student_ans` text NOT NULL,
  `grade` varchar(255) NOT NULL,
  `prof_comments` varchar(700) NOT NULL,
  `auto_grader_comments` varchar(2000) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=211 ;

--
-- Dumping data for table `student_ans_exam`
--

INSERT INTO `student_ans_exam` (`uniq_exam_solve_id`, `ucid`, `exam_id`, `auto_grader_status`, `graded_status`, `visible_status`, `student_ans`, `grade`, `prof_comments`, `auto_grader_comments`) VALUES
(209, 'stud', 70, 1, 0, 1, 'ZGVmIG91YmxlSXQoYSk6CiAgICByZXR1cm4gYSoy,ZGVmIG9wKGEsYixjKToKICAgIGlmKGEgPT0gJyonKToKICAgICAgICByZXR1cm4gYipjCiAgICBpZihhID09ICcvJyk6CiAgICAgICAgcmV0dXJuIGIvYw==,ZGVmIHN0cmluZ011bHRpcGx5KGEsYik6CiAgICByZXR1cm4gYSpi', '[[0,6.25,6.25,0,6.25],[12.5,12.5,12.5,0,6.25],[5,5,5,0,0]]', '["","","Much better"]', '[[{"function":false,"compile":true,"print":false,"return":true,"keywords expected":[""],"keywords hit":[],"test cases passed":2,"score":[0,6.25,6.25,0,6.25],"python":[{"function":"doubleIt","expected":"4","result":"4","points":3.125},{"function":"doubleIt","expected":"6","result":"6","points":3.125}]}],[{"function":true,"compile":true,"print":false,"return":true,"keywords expected":[""],"keywords hit":[],"test cases passed":2,"score":[12.5,12.5,12.5,0,6.25],"python":[{"function":"op","expected":"5","result":"None","points":0},{"function":"op","expected":"1","result":"None","points":0},{"function":"op","expected":"12","result":"12","points":3.125},{"function":"op","expected":"3","result":"3","points":3.125}]}],[{"function":true,"compile":true,"print":false,"return":true,"keywords expected":["for"],"keywords hit":[],"test cases passed":2,"score":[5,5,5,0,0],"python":[{"function":"stringMultiply","expected":"hihi","result":"hihi","points":0},{"function":"stringMultiply","expected":"hellohellohello","result":"hellohellohello","points":0}]}]]'),
(210, 'stud', 71, 1, 0, 1, 'ZGVmIG91YmxlSXQoYSk6CiAgICByZXR1cm4gYSoy,ZGVmIHN0cmluZ011bHQoYSxiKToKICAgIHJldHVybiBhKmI=,ZGVmIHBsdXNUZW4oYSk6CiAgICBmb3IgaSBpbiByYW5nZSgxMCk6CiAgICAgICAgYSA9IGErMQogICAgcmV0dXJuIGE=', '[[5,6.25,6.25,0,6.25],[6.25,6.25,6.25,0,6.25],[10,10,10,0,10]]', '["mashaallah u did good i guess","","u cheated"]', '[[{"function":false,"compile":true,"print":false,"return":true,"keywords expected":[""],"keywords hit":[],"test cases passed":2,"score":[5,6.25,6.25,0,6.25],"python":[{"function":"doubleIt","expected":"4","result":"4","points":3.125},{"function":"doubleIt","expected":"6","result":"6","points":3.125}]}],[{"function":true,"compile":true,"print":false,"return":true,"keywords expected":[""],"keywords hit":[],"test cases passed":2,"score":[6.25,6.25,6.25,0,6.25],"python":[{"function":"stringMult","expected":"hellohello","result":"hellohello","points":3.125},{"function":"stringMult","expected":"hihihi","result":"hihihi","points":3.125}]}],[{"function":true,"compile":true,"print":false,"return":true,"keywords expected":["for"],"keywords hit":["for"],"test cases passed":2,"score":[10,10,10,0,10],"python":[{"function":"plusTen","expected":"20","result":"20","points":5},{"function":"plusTen","expected":"25","result":"25","points":5}]}]]');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `exam_list`
--
ALTER TABLE `exam_list`
 ADD PRIMARY KEY (`exam_id`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
 ADD PRIMARY KEY (`ucid`);

--
-- Indexes for table `question_bank`
--
ALTER TABLE `question_bank`
 ADD PRIMARY KEY (`qid`);

--
-- Indexes for table `student_ans_exam`
--
ALTER TABLE `student_ans_exam`
 ADD PRIMARY KEY (`uniq_exam_solve_id`), ADD KEY `ucid` (`ucid`), ADD KEY `exam_id` (`exam_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `exam_list`
--
ALTER TABLE `exam_list`
MODIFY `exam_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=72;
--
-- AUTO_INCREMENT for table `question_bank`
--
ALTER TABLE `question_bank`
MODIFY `qid` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=80;
--
-- AUTO_INCREMENT for table `student_ans_exam`
--
ALTER TABLE `student_ans_exam`
MODIFY `uniq_exam_solve_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=211;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `student_ans_exam`
--
ALTER TABLE `student_ans_exam`
ADD CONSTRAINT `student_ans_exam_ibfk_1` FOREIGN KEY (`ucid`) REFERENCES `login` (`ucid`),
ADD CONSTRAINT `student_ans_exam_ibfk_2` FOREIGN KEY (`exam_id`) REFERENCES `exam_list` (`exam_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
